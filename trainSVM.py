from numpy import require
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
import extreact
import joblib
import pickle
from sklearn.svm import LinearSVC
from sklearn.metrics import classification_report,confusion_matrix
import os
X_train, X_test, y_train, y_test = train_test_split(extreact.X,extreact.y, test_size = 0.2,random_state=4)



svc_model = LinearSVC()
model = DecisionTreeClassifier()
svc_model.fit(X_train, y_train)
y_predict = svc_model.predict(X_test)



from sklearn.metrics import classification_report, confusion_matrix
cm = confusion_matrix(y_test, y_predict)

# in ra confusion matrix
print("confusion matrix",cm)
plt.imshow(cm)
plt.show()

sns.heatmap(cm, annot = True, fmt = "d")
print(classification_report(y_test, y_predict))
Model_prediction = svc_model.predict(X_test[0:50])
# print(Model_prediction)
Model_trueLabel = y_test[0:50]
# print(Model_trueLabel)
model_path = "data/models/completed_model_4.joblib"

if not os.path.isdir(os.path.split(model_path)[0]):
            os.makedirs(os.path.split(model_path)[0])
joblib.dump(svc_model, model_path)
print ("Classifier saved to {}".format(model_path))

