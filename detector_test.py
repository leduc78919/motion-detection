from ast import Return
from genericpath import exists
from cv2 import COLOR_BGR2GRAY, COLOR_RGB2GRAY
import matplotlib.pyplot as plt
import time
import numpy as np 
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
import imutils
from skimage.feature import hog
import joblib
import cv2 as cv
#from config import *
from skimage import color
import matplotlib.pyplot as plt 
import os 
import glob
# def crop_image(im):
#     #im = cv.imread(img,cv.IMREAD_GRAYSCALE)
#     contours, _ = cv.findContours(im, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
#     i = 0;
#     for cnt in contours:
#         x,y,w,h = cv.boundingRect(cnt)
#         if w*h < 900:
#             continue
#         cropped = im[y:y+h,x:x+w]
#         cropped = cv.resize(cropped,(64,64))
#         cv.imwrite('data/image_crop/cropped_image_' + str(i) + '.png', cropped)    
#         i = i +1


def cropDetect(im,frame):
    svc_model = joblib.load('data/models/completed_model_4.joblib')
    contours, _ = cv.findContours(im, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    i = 0;
    for cnt in contours:
        x,y,w,h = cv.boundingRect(cnt)
        if w*h > 700:
            cropped = frame[y:y+h,x:x+w]
            cropped = cv.cvtColor(cropped,COLOR_RGB2GRAY)
            cropped = cv.resize(cropped,(64,128))
            hog_feat_sample,hog_image_sample = hog(cropped, orientations = 11, pixels_per_cell= (16,16), cells_per_block = (2,2),transform_sqrt = False,visualize = True,feature_vector = True)
                #fd = fd.reshape(1, -1)
            fd = hog_feat_sample.reshape(1,-1)
            pred = svc_model.predict(fd)
            print('svc_model.predict(fd): ', svc_model.decision_function(fd))
            if pred == 1:     
                if svc_model.decision_function(fd) > 1.1 :
                    cv.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                    cv.putText(frame, "Status: {}".format('Movement'), (10, 20), cv.FONT_HERSHEY_SIMPLEX,
                        1, (255, 0, 0), 3)
        i = i +1
    return frame
      
        

        
def motionDetection():
    #cap = cv.VideoCapture("test-7.mp4")
    cap = cv.VideoCapture("video_self/test2.mp4")
   # cap = cv.VideoCapture("data/video_test_5.mp4")
    #cap = cv.VideoCapture("pedestrians.avi")
    ret, frame1 = cap.read()
    ret, frame2 = cap.read()

    while cap.isOpened():
        diff = cv.absdiff(frame1, frame2) 
        diff_gray = cv.cvtColor(diff, cv.COLOR_BGR2GRAY)
        blur = cv.GaussianBlur(diff_gray, (5, 5), 0) 
        _, thresh = cv.threshold(blur, 20, 255, cv.THRESH_BINARY)
      
        dilated = cv.dilate(thresh, None, iterations=3)
        #frame1 = detector(dilated,frame1)
        frame1 = cropDetect(dilated,frame1)
        cv.imshow("Video", frame1)
        #cv.imshow("Video", dilated)
        frame1 = frame2
        ret, frame2 = cap.read()
        if cv.waitKey(50) == 27:
            break

    cap.release()
    cv.destroyAllWindows()


if __name__ == "__main__":
    motionDetection()

