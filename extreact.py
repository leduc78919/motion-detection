import cv2
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from skimage.feature import hog
import glob
import os
pos = glob.glob('data/image/pos-1/*.bmp')
neg = glob.glob('data/image/neg-1/*.png')

#pos = glob.glob('data/image/1/*.png')
#neg = glob.glob('data/image/0/*.png')

pos_hog_accum = []
for i in range(len(pos)):
  # print(i)
  image_color = cv2.imread(pos[i])
  image_color = cv2.resize(image_color,(64,128))
  image_gray  = cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
  
  blur = cv2.GaussianBlur(image_gray, (5, 5), 0) 
  _, thresh = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)
      
  dilated = cv2.dilate(thresh, None, iterations=3)

  pos_hog_feature,pos_hog_img = hog(image_gray,
                                    orientations = 11,
                                    pixels_per_cell = (16,16),
                                    cells_per_block = (2,2),
                                    transform_sqrt = False,
                                    visualize = True,
                                    feature_vector = True)
  pos_hog_accum.append(pos_hog_feature)


X_pos = np.vstack(pos_hog_accum).astype(np.float64)
y_pos = np.ones(len(X_pos))

# print(X_pos.shape)
# print(y_pos.shape)



neg_hog_accum = []

# print(len(neg))
for i in range(len(neg)):
  # print(i)
  image_color = cv2.imread(neg[i])
  image_color = cv2.resize(image_color,(64,128))
  image_gray  = cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)

  neg_hog_feature,pos_hog_img = hog(image_gray,
                                    orientations = 11,
                                    pixels_per_cell = (16,16),
                                    cells_per_block = (2,2),
                                    transform_sqrt = False,
                                    visualize = True,
                                    feature_vector = True)
  neg_hog_accum.append(neg_hog_feature)

X_neg = np.vstack(neg_hog_accum).astype(np.float64)
y_neg = np.zeros(len(X_neg))

X= np.vstack((X_pos, X_neg))
y= np.hstack((y_pos, y_neg))
