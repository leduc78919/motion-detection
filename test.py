from genericpath import exists
import numpy as np 
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
import imutils
from skimage.feature import hog
import joblib
import cv2
#from config import *
from skimage import color
import matplotlib.pyplot as plt 
import os 
import glob
import time
#import trainSVM


def sliding_window(image, window_size, step_size):
    '''
    This function returns a patch of the input 'image' of size 
    equal to 'window_size'. The first image returned top-left 
    co-ordinate (0, 0) and are increment in both x and y directions
    by the 'step_size' supplied.

    So, the input parameters are-
    image - Input image
    window_size - Size of Sliding Window 
    step_size - incremented Size of Window

    The function returns a tuple -
    (x, y, im_window)
    '''
    for y in range(0, image.shape[0], step_size[1]):
        for x in range(0, image.shape[1], step_size[0]):
            yield (x, y, image[y: y + window_size[1], x: x + window_size[0]])

def detector(filename):
    start = time.time()
    im = cv2.imread(filename)
    #im = cv2.resize(im,(64,64))
    im = imutils.resize(im, width = min(400, im.shape[1]))
    min_wdw_sz = (64,128)
    step_size = (64,64)
    downscale = 1.25
    svc_model = joblib.load('data/models/completed_model.joblib')
    #clf = joblib.load(os.path.join(model_path, 'svm.model'))

    #List to store the detections
    detections = []
    #The current scale of the image 
    scale = 0
    for im_scaled in pyramid_gaussian(im, downscale = downscale):
        #The list contains detections at the current scale
        if im_scaled.shape[0] < min_wdw_sz[1] or im_scaled.shape[1] < min_wdw_sz[0]:
            break
        for (x, y, im_window) in sliding_window(im_scaled, min_wdw_sz, step_size):
            if im_window.shape[0] != min_wdw_sz[1] or im_window.shape[1] != min_wdw_sz[0]:
                continue
            #im_window = color.rgb2gray(im_window)
            im_window = cv2.resize(im_window,(64,64))
            im_window = color.rgb2gray(im_window)
            #im_window_R= im_window[:,:,0]
            hog_feat_sample,hog_image_sample = hog(im_window, orientations = 11, pixels_per_cell= (16,16), cells_per_block = (2,2),transform_sqrt = False,visualize = True,feature_vector = True)
            #fd = fd.reshape(1, -1)
            fd = hog_feat_sample.reshape(1,-1)
            pred = svc_model.predict(fd)
            print('svc_model.predict(fd): ', svc_model.decision_function(fd))
            if pred == 1:
                
                if svc_model.decision_function(fd)> 1.2:
                    detections.append((int(x * (downscale**scale)), int(y * (downscale**scale)), svc_model.decision_function(fd), 
                    int(min_wdw_sz[0] * (downscale**scale)),
                    int(min_wdw_sz[1] * (downscale**scale))))
                 

            
        scale += 1
    clone = im.copy()

    for (x_tl, y_tl, _, w, h) in detections:
        # print(x_tl,y_tl,w,h)
        cv2.rectangle(im, (x_tl, y_tl), (x_tl + w, y_tl + h), (0, 255, 0), thickness = 2)
    rects = np.array([[x, y, x + w, y + h] for (x, y, _, w, h) in detections])
    sc = [score[0] for (x, y, score, w, h) in detections]
    # print ("sc: ", sc)
    sc = np.array(sc)
    pick = non_max_suppression(rects, probs = sc, overlapThresh = 0.3)
    print ("shape, ", len(pick))

    for(xA, yA, xB, yB) in pick:
        cv2.rectangle(clone, (xA, yA), (xB, yB), (0, 255, 0), 2)
        # print((xA, yA), (xB, yB))
    end = time.time()
    # print(end-start)
    exists(0)
    plt.axis("off")
    plt.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
    plt.title("Raw Detection before NMS")
    plt.show()

    plt.axis("off")
    plt.imshow(cv2.cvtColor(clone, cv2.COLOR_BGR2RGB))
    plt.title("Final Detections after applying NMS")
    plt.show()
#detector('image_test/image_crop/cropped_image_4.png')
imgtest = glob.glob('image_test/*.jpg')
for i in range(len(imgtest)):
    detector(imgtest[i])
#detector('data/image_crop/cropped_image_0.png')


