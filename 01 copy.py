import cv2 as cv
from cv2 import COLOR_GRAY2BGR
import numpy as np
#FROM MATPLOTLIB IMPORT PYPLOT AS PLT
import matplotlib.pyplot as plt

def motionDetection():
    cap = cv.VideoCapture("car_highway.mp4")
    ret, frame1 = cap.read()
    ret, frame2 = cap.read()

    while cap.isOpened():
        diff = cv.absdiff(frame1, frame2) 
        # plt.figure
        # plt.imshow(diff)
        # plt.show()
        # exit()
        diff_gray = cv.cvtColor(diff, cv.COLOR_BGR2GRAY)
        
        # plt.figure
        # plt.imshow(diff_gray)
        # plt.show()
        blur = cv.GaussianBlur(diff_gray, (5, 5), 0) 
        _, thresh = cv.threshold(blur, 20, 255, cv.THRESH_BINARY)
      
        dilated = cv.dilate(thresh, None, iterations=3)
        
        dilated = cv.cvtColor(dilated,COLOR_GRAY2BGR)
        # plt.figure
        # plt.imshow(dilated)
        # plt.show()
        # cv.imwrite('image_test/car_color.png', dilated)

        contours, _ = cv.findContours(dilated, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        
        # plt.figure
        # plt.imshow(dilated)
        # plt.show()
        # cv.imwrite('car.highway.png', dilated)
        # exit()
        for contour in contours:

            (x, y, w, h) = cv.boundingRect(contour)

            if cv.contourArea(contour) > 900:
                continue
            cv.rectangle(frame1, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv.putText(frame1, "Status: {}".format('Movement'), (10, 20), cv.FONT_HERSHEY_SIMPLEX,
                       1, (255, 0, 0), 3)

        cv.drawContours(frame1, contours, -1, (0, 255, 0), 2)
        cv.imshow("Video", dilated)
        frame1 = frame2
        ret, frame2 = cap.read()

        if cv.waitKey(50) == 27:
            break

    cap.release()
    cv.destroyAllWindows()


if __name__ == "__main__":
    motionDetection()